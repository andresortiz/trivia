import {  StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';

export default function Resultados({navigation, route}) {

  const { score, preguntas, respuestas } = route.params;

  console.log(preguntas)

  return (
    <View style={styles.container2}>
		<Text style={styles.titulo} >Lograste</Text>
		<Text style={styles.titulo2} >{score}/10</Text>
		<Text style={styles.texto} > Tus respuestas</Text>

		{preguntas.map((d, index, array) => {
			return (
				<View style={styles.box}>
					<Text style={styles.item}>{d.text}</Text>
					<Text style={styles.respuesta}>{d.answers[respuestas[index]-1]?.text} { (d.answers[respuestas[index]-1]?.number == d.correctAnswer) ? <Ionicons name="md-checkmark-circle" size={20} color="green" /> : <Ionicons name="md-close-circle" size={20} color="red" />  } </Text>
				</View>
				);
			})
		}

		<TouchableOpacity
			onPress={() =>
			navigation.navigate('Inicio', { name: 'Inicio' })
			}
			style={styles.boton}
		>
			<Text style={styles.item2}>JUGAR OTRA VEZ?</Text>
		</TouchableOpacity>

    </View>
  );
}

const styles = StyleSheet.create({
	container: {
		flex: 1, backgroundColor: '#eaeaea', alignItems: 'center', justifyContent: 'center', padding: 50,
	},
	container2: {
		flex: 1, backgroundColor: '#eaeaea', alignItems: 'center', padding: 0,  justifyContent: 'flex-start',
	},
	titulo: {
		fontSize:30, textAlign: 'center', fontWeight:'bold'
	},
	titulo2: {
		fontSize:30, textAlign: 'center', fontWeight:'bold'
	},
	texto: {
		fontSize:30, textAlign: 'center', color: '1d1d1d'
	},
	texto2: {
		fontSize:30, textAlign: 'center', position:'absolute', top:'70%'
	},
	boton: {
		// position:'absolute', bottom:'5%'
	},
	botonText: {
		fontSize:30, fontWeight:'bold',
  	},
	item: {
			marginTop: 5,
			padding: 3,
			fontSize: 20,
			fontWeight:'bold',
			justifyContent: 'flex-start',
			width:'100%',
			textAlign: 'center',
			color: '#1d1d1d'
	},

	respuesta: {
			marginTop: 5,
			padding: 3,
			fontSize: 20,
			justifyContent: 'flex-start',
			width:'100%',
			textAlign: 'center',
			color: '#808080'
	},
	item2: {
			marginTop: 5,
			padding: 3,
			justifyContent: 'flex-start',
			width:'100%',
			textAlign: 'center',
			fontSize:30, 
			fontWeight:'bold',
	},
	box: {
			marginTop: 5,
			padding: 3,
			borderColor: '#a6a6a6',  
			borderWidth:1, 
			border: 'solid 1px',
			width:'100%',
			textAlign: 'center'
	}
});

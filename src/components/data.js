var data = []
export default data = {
        questions: [
                    {
                        text: "¿Como se llama la ciudad donde viven los Simpsons?",
                        correctAnswer: 1,
                        answers: [
                            {
                            number: 1,
                            text: "Springfield"
                            },
                            {
                            number: 2,
                            text: "Shelbyville"
                            },
                            {
                            number: 3,
                            text: "Knoxville"
                            },
                            {
                            number: 4,
                            text: "No se sabe"
                            }
                        ]
                    },
                    {
                        text: "¿Como se llama el amigo de Lenny?",
                        correctAnswer: 3,
                        answers: [
                        {
                        number: 1,
                        text: "Joe"
                        },
                        {
                        number: 2,
                        text: "Gilbert"
                        },
                        {
                        number: 3,
                        text: "Carl"
                        },
                        {
                        number: 4,
                        text: "Krusty"
                        }
                        ]
                    },
                    {
                        text: "¿Cuantas personas viven en la casa de Los Simpsons?",
                        correctAnswer: 2,
                        answers: [
                        {
                        number: 1,
                        text: "4"
                        },
                        {
                        number: 2,
                        text: "5"
                        },
                        {
                        number: 3,
                        text: "6"
                        },
                        {
                        number: 4,
                        text: "7"
                        }
                        ]
                    },
                    {
                        text: "¿Como se llaman las hermanas de Marge?",
                        correctAnswer: 3,
                        answers: [
                        {
                        number: 1,
                        text: "Patty y Zulma"
                        },
                        {
                        number: 2,
                        text: "Zulma y Penelope"
                        },
                        {
                        number: 3,
                        text: "Patty y Selma"
                        },
                        {
                        number: 4,
                        text: "Selma y Penelope"
                        }
                        ]
                    },
                    {
                        text: "¿Quién es el lider de la mafia en Springfield?",
                        correctAnswer: 1,
                        answers: [
                        {
                        number: 1,
                        text: "Tony el gordo"
                        },
                        {
                        number: 2,
                        text: "Krusty"
                        },
                        {
                        number: 3,
                        text: "Snake"
                        },
                        {
                        number: 4,
                        text: "Bob patiño"
                        }
                        ]
                    },
                    {
                        text: "¿Con quién vive Skinner?",
                        correctAnswer: 2,
                        answers: [
                        {
                        number: 1,
                        text: "Solo"
                        },
                        {
                        number: 2,
                        text: "Con su madre"
                        },
                        {
                        number: 3,
                        text: "Con Edna Kravappel"
                        },
                        {
                        number: 4,
                        text: "Con Willy"
                        }
                        ]
                    },
                    {
                        text: "¿Cual es el segundo nombre de Homero?",
                        correctAnswer: 2,
                        answers: [
                        {
                        number: 1,
                        text: "James"
                        },
                        {
                        number: 2,
                        text: "Jay"
                        },
                        {
                        number: 3,
                        text: "Jade"
                        },
                        {
                        number: 4,
                        text: "John"
                        }
                        ]
                    },
                    {
                        text: "¿A que religión pertenece Krusty el payaso?",
                        correctAnswer: 2,
                        answers: [
                        {
                        number: 1,
                        text: "Cristianismo"
                        },
                        {
                        number: 2,
                        text: "Judaísmo"
                        },
                        {
                        number: 3,
                        text: "Budismo"
                        },
                        {
                        number: 4,
                        text: "Arabe"
                        }
                        ]
                    },
                    {
                        text: "¿Que proponia la propuesta 24?",
                        correctAnswer: 1,
                        answers: [
                        {
                        number: 1,
                        text: "Expulsar los inmigrantes ilegales de Springfield"
                        },
                        {
                        number: 2,
                        text: "Prohibir el consumo de alcohol"
                        },
                        {
                        number: 3,
                        text: "Prohibir el consumo de azucar"
                        },
                        {
                        number: 4,
                        text: "Expulsar a los osos de Springfield"
                        }
                        ]
                    },
                    {
                        text: "¿Quien es el peor enemigo de Milhouse?",
                        correctAnswer: 1,
                        answers: [
                        {
                        number: 1,
                        text: "Nelson"
                        },
                        {
                        number: 2,
                        text: "Bart"
                        },
                        {
                        number: 3,
                        text: "Bob el patiño"
                        },
                        {
                        number: 4,
                        text: "Krusty"
                        }
                        ]
                    }
    ]
}

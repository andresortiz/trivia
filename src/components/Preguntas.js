import React, { useEffect, useState } from 'react';
import { FlatList, Button, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import data from './data'

export default function Preguntas({navigation}) {
  
    const [isLoading, setLoading] = useState(false);
    const [preguntas, setPreguntas] = useState([]);
    const [respuestas, setRespuestas] = useState([]);
	
	
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0)
    const [currentOptionSelected, setCurrentOptionSelected] = useState(null);
    const [correctOption, setCorrectOption] = useState(null);
    const [isOptionsDisabled, setIsOptionsDisabled] = useState(false);
    const [score, setScore] = useState(0)
    const [showFinalButton, setShowFinalButton] = useState(false)
	var listado = []


	const validateAnswer = (selectedOption) => {

        let correct_option = preguntas[currentQuestionIndex]['correctAnswer'];
		// Verificamos que la respuesta sea correcta
        setCurrentOptionSelected(selectedOption);
        setCorrectOption(correct_option);
		//Deshabilitamos las opciones de la votación.
        setIsOptionsDisabled(true);
        if(selectedOption==correct_option){
			// actualizamos el score
            setScore(score+1)
        }
        respuestas.push(selectedOption)
		siguientePregunta()
    }
    const siguientePregunta = () => {
        if(currentQuestionIndex== preguntas.length-1){
			//Si es la ultima pregunta, mostramos el boton de finalizar
            setShowFinalButton(true);
        }else{
            setCurrentQuestionIndex(currentQuestionIndex+1);
            setCurrentOptionSelected(null);
            setCorrectOption(null);
            setIsOptionsDisabled(false);
            setShowFinalButton(false);
        }
        
		
    }

	function goResultados(){
		navigation.navigate('Resultados', { score: score , preguntas: preguntas, respuestas: respuestas})
	}

	async function setString() {
		listado = await AsyncStorage.setItem('preguntas', JSON.stringify(data));
		setLoading(false);
		return listado;
	}
	
	async function getString() {
		let result = await AsyncStorage.getItem('preguntas');
		try {
			listado = JSON.parse(result);
			setPreguntas(listado.questions);
			setLoading(false);
		  } catch (err) {
			console.error('Error: ', err.message);
		  }
		return listado;
	}


	const getPreguntas = () => {
		try {
			fetch('https://softec.com.py/preguntas.php')
			.then((response) => response.json())
			.then((json) => {
				setPreguntas(json);
			})
			.catch((error) => 
			{
				console.log(error); 
				// Si la Api no funciona, utilizamos datos de ejemplo para ver el funcionamiento de la app.
				setString();
				getString();
			}
			)
			.finally(() => setLoading(false));
		} catch (error) {
			console.log('ocurrio un error:' + error);
		}
	}
	useEffect(() => {
		
		getPreguntas();
		setLoading(true);

    }, []);

    return(
        <View style={styles.container}>
           
			
            {isLoading ? <Text style={styles.texto} >Cargando preguntas...</Text> :
            (
				<View style={styles.container2}>
					<View style={styles.pregunta}>
						<Text style={styles.texto} >{preguntas[currentQuestionIndex]?.text}</Text>
					</View>

					<TouchableOpacity
						onPress={()=> validateAnswer(preguntas[currentQuestionIndex]?.answers[0]?.number )}
                        disabled={isOptionsDisabled}
						style={styles.item}
						>
						<Text style={styles.botonText2}>{preguntas[currentQuestionIndex]?.answers[0]?.text }</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={()=> validateAnswer(preguntas[currentQuestionIndex]?.answers[1]?.number )}
                        disabled={isOptionsDisabled}
						style={styles.item}
						>
						<Text style={styles.botonText2}>{preguntas[currentQuestionIndex]?.answers[1]?.text }</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={()=> validateAnswer(preguntas[currentQuestionIndex]?.answers[2]?.number )}
                        disabled={isOptionsDisabled}
						style={styles.item}
						>
						<Text style={styles.botonText2}>{preguntas[currentQuestionIndex]?.answers[2]?.text }</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={()=> validateAnswer(preguntas[currentQuestionIndex]?.answers[3]?.number )}
                        disabled={isOptionsDisabled}
						style={styles.item}
						>
						<Text style={styles.botonText2}>{preguntas[currentQuestionIndex]?.answers[3]?.text }</Text>
					</TouchableOpacity>
					
				</View>
				
            )};

			<View>
			{	  
				!showFinalButton ? 
					
					<Text style={styles.count}>{currentQuestionIndex+1} de 10</Text> :
					
					(

						<TouchableOpacity
							onPress={goResultados}
							style={styles.item}
							>
							<Text style={styles.botonText}>FINALIZAR</Text>
						</TouchableOpacity>

					)
			};
			</View>
            
        </View>
    );
};

	const styles = StyleSheet.create({
		container: {
		flex: 1, backgroundColor: '#eaeaea', alignItems: 'center', justifyContent: 'center', padding: 20
		},
		container2: {
			flex: 1, backgroundColor: '#eaeaea', alignItems: 'center', padding: 0,  justifyContent: 'flex-start',
		},
		titulo: {
		fontSize:30, textAlign: 'center', fontWeight:'bold', position:'absolute', top:'5%'
		},
		pregunta: {
		fontSize:30, textAlign: 'center', borderColor: '#ccc', border: 'solid 1px', paddingBottom: 300, marginBottom:30,paddingTop:20, paddingHorizontal: 20,
		},
		texto: {
		fontSize:30, textAlign: 'center'
		},
		count: {
		fontSize:25, textAlign: 'center'
		},
		texto2: {
		fontSize:30, textAlign: 'center', 
		},
		boton: {
		position:'absolute', bottom:'5%', borderColor: '#ccc', border: 'solid 1px', 
		},
		botonText: {
		fontSize:30, fontWeight:'bold',
		},
		boton2: {
			position:'absolute', bottom:'5%'
		},
		botonText2: {
			fontSize:30, fontWeight:'bold',
		},
		item: {
			marginTop: 10,
			padding: 20,
			fontSize: 15,
			justifyContent: 'flex-start',
			borderColor: '#ccc',  
			borderWidth:1, 
			border: 'solid 1px',
			width:'100%',
			textAlign: 'center'
		}
	});


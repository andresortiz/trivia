import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, TouchableOpacity,Text, View} from 'react-native';

export default function Inicio({navigation}) {

	function goPreguntas(){
		navigation.navigate('Preguntas', { name: 'Preguntas' })
	}
   
	return (
		<View style={styles.container}>
			<Text style={styles.titulo} >Bienvenido al desafío de Trivia!</Text>
			<Text style={styles.texto}>Son 10 preguntas de opción múltiple</Text>
			<Text style={styles.texto2}>Podes lograr 100%?</Text>

			<TouchableOpacity
				onPress={goPreguntas}
				style={styles.boton}
				>
				<Text style={styles.botonText}>INICIAR</Text>
			</TouchableOpacity>

		<StatusBar style="auto" />
		</View>
	);
}

const styles = StyleSheet.create({
  container: {
    flex: 1, backgroundColor: '#eaeaea', alignItems: 'center', justifyContent: 'center', padding: 50,
  },
  titulo: {
    fontSize:30, textAlign: 'center', fontWeight:'bold', position:'absolute', top:'10%'
  },
  texto: {
    fontSize:30, textAlign: 'center', position:'absolute', top:'40%'
  },
  texto2: {
    fontSize:30, textAlign: 'center', position:'absolute', top:'70%'
  },
  boton: {
    position:'absolute', bottom:'5%'
  },
  botonText: {
    fontSize:30, fontWeight:'bold',
  },
});

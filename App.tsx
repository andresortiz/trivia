// Lic. Andrés Ortiz
// ortizvillalba@gmail.com

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';

import Inicio from './src/components/Inicio';
import Preguntas from './src/components/Preguntas';
import Resultados from './src/components/Resultados';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Inicio"
          component={Inicio}
          options={{ title: 'Trivia' }}
        />
        <Stack.Screen
          name="Preguntas"
          component={Preguntas}
        />
        <Stack.Screen
          name="Resultados"
          component={Resultados}
          options={{ title: 'Resultado' }}
        />
      </Stack.Navigator>  
    </NavigationContainer>
  );
}

export default App


